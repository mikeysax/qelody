FactoryGirl.define do
  factory :course do
    title "Baking Cakes"
    description "How to bake a cake."
    cost 20.0
    user_id 1
    # image { Rack::Test::UploadedFile.new(File.join(Rails.root, 'app', 'assets', 'images', 'hero.jpg')) }
    # after :create do |b|
    #   b.update_column(:image, "foo/bar/baz.png")
    # end
  end
end
