require 'rails_helper'

describe Course do
  it "has a valid factory" do
    expect(FactoryGirl.create(:course)).to be_valid
  end
  it "is deleted" do
    course = FactoryGirl.create(:course)
    course.destroy
    expect(Course.find_by(id: course.id)).to be_nil
  end
end
