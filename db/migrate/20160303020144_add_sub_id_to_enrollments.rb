class AddSubIdToEnrollments < ActiveRecord::Migration
  def change
    add_column :enrollments, :subscription_id, :string
  end
end
