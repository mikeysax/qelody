class AddInstrumentToCourses < ActiveRecord::Migration
  def change
    add_column :courses, :instrument, :string
    add_index :courses, :instrument
  end
end
