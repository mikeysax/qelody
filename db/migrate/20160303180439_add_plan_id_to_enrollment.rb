class AddPlanIdToEnrollment < ActiveRecord::Migration
  def change
    add_column :enrollments, :plan_id, :string
  end
end
