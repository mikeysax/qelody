class Enrollment < ActiveRecord::Base
   belongs_to :course
   belongs_to :user

  def self.create_stripe_plan(user, course, enroll, amount)
    plan = Stripe::Plan.create({
      :amount => amount,
      :interval => 'month',
      :name => course.title,
      :currency => 'usd',
      :id => course.title.to_s + "_CourseID-" + course.id.to_s + "_UserID-" + user.id.to_s + "_UserNAME-" + user.username.to_s},
      {:stripe_account => course.user.uid}
    )
    enroll.update(plan_id: plan.id)
  end

  def self.create_stripe_customer(user, course, token)
    customer = Stripe::Customer.create({
      :email => user.email,
      :source => token,
      :description => "Qelody Subscriber for #{course.title}"},
      {:stripe_account => course.user.uid}
    )
    user.update(customer_id: customer.id)
  end

  def self.create_stripe_subscription(user, course, enroll)
    customer = Stripe::Customer.retrieve(user.customer_id.to_s, :stripe_account => course.user.uid)
    subscription = customer.subscriptions.create({
      :plan => enroll.plan_id,
      :application_fee_percent => 25},
      {:stripe_account => course.user.uid}
    )
    enroll.update(subscription_id: subscription.id)
  end

  def self.destroy_enrollment_and_stripe_subscription_and_plan(user, course, enroll)
    customer = Stripe::Customer.retrieve(user.customer_id.to_s, :stripe_account => course.user.uid)
    customer.subscriptions.retrieve(enroll.subscription_id.to_s).delete
    plan = Stripe::Plan.retrieve(enroll.plan_id.to_s, :stripe_account => course.user.uid)
    plan.delete
    user.enrollments.destroy(enroll)
  end
end
