class LessonsController < ApplicationController
   before_action :authenticate_user!
   before_action :require_authorized_for_current_lesson, :only => [:show]

    def show
    end

    def destroy
      @lesson = Lesson.find(params[:id])
      if @lesson.user != current_user
        render :text => 'Unauthorized', :status => :unauthorized and return
      end
      @course = @lesson.section.course
      @lesson.destroy
      redirect_to instructor_course_path(@course.id)
    end


   private

    helper_method :current_lesson
    def current_lesson
      @current_lesson ||= Lesson.find(params[:id])
    end

    def require_authorized_for_current_lesson
      if current_user.enrolled_in?(current_lesson.section.course)
         return
      else
         redirect_to course_path, :alert => 'You Must Enroll In Course'
      end
    end

end
