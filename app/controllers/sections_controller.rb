class SectionsController < ApplicationController
   before_action :authenticate_user!

    def destroy
      @section = Section.find(params[:id])

      if @section.course.user != current_user
        render :text => 'Unauthorized', :status => :unauthorized and return
      end
      @course = @section.course
      @section.destroy
      redirect_to instructor_course_path(@course.id)
    end

end
