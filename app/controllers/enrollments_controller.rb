class EnrollmentsController < ApplicationController
  before_action :authenticate_user!

  def create
    if current_course.premium?
      current_user.enrollments.create(:course => current_course)
      @amount = (current_course.cost * 100).to_i # Amount in cents
      Enrollment.create_stripe_plan(current_user, current_course, current_enrollment, @amount)
      token = params[:stripeToken]
      Stripe.api_key = ENV['SECRET_KEY']
      Enrollment.create_stripe_customer(current_user, current_course, token) if current_user.customer_id.nil?
      Enrollment.create_stripe_subscription(current_user, current_course, current_enrollment)
    else
      current_user.enrollments.create(:course => current_course)
    end
    redirect_to course_path(current_course)
  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to root_path
  end

  def destroy
    Stripe.api_key = ENV['SECRET_KEY']
    Enrollment.destroy_enrollment_and_stripe_subscription_and_plan(current_user, current_course, current_enrollment) if current_course.premium?
    current_user.enrollments.destroy(:course => current_course) if current_course.free?
    redirect_to course_path(current_course)
  end

  private

  def current_course
    @current_course ||= Course.find(params[:course_id])
  end

  def current_enrollment
    @current_enrollment ||= Enrollment.find_by(user_id: current_user.id, course_id: current_course.id)
  end
end
