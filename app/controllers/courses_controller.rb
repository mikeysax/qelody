class CoursesController < ApplicationController
  before_action :authenticate_user!, only: [:destroy]

  def index
    @courses = Course.all
  end

  def show
    @course = Course.find(params[:id])
    @enrollment = Enrollment.find_by(course_id: @course.id, user_id: current_user.id)
  end


  def destroy
    @course = Course.find(params[:id])
    if @course.user != current_user
      render :text => 'Unauthorized', :status => :unauthorized and return
    end
    @course.destroy
    redirect_to root_path
  end

end
